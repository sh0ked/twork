# coding=utf-8
from sys import argv


def string_modify(line):
    return ''.join(sorted(line.decode('utf-8').lower()))


def get_lines(file_name):
    try:
        f = open('%s' % file_name, 'r')
        lines = f.readlines()
        f.close()

        return [str(i.split('\n')[0]) for i in lines]
    except IOError:
        raise EnvironmentError('\nФайла с названием %s не нашлось.' % file_name)


def save_lines(new_file_name, newlines):
    newfile = open('%s' % new_file_name, 'wa+')
    for key in newlines:
        newfile.writelines('%s %s\n' % (key.encode('utf-8'), ', '.join(newlines[key])))
    newfile.close()

    print '\n', '*' * 10
    print u'Создан файл %s.' % new_file_name
    print u'Содержимое файла %s:\n' % new_file_name
    for key in newlines:
        print '%s ' % ', '.join(newlines[key])
    print '\n', '*' * 10


def anagram():
    if len(argv) == 2:
        file_name = argv[1]
        new_file_name = '%s_anagram' % file_name

        lines = get_lines(file_name)
        newlines = {}

        for line in lines:
            if line is not '':
                mod_line = string_modify(line)
                if mod_line in newlines.keys():
                    newlines[mod_line].append(line)
                else:
                    newlines[mod_line] = [line]

        save_lines(new_file_name, newlines)
    else:
        raise AttributeError('\nПрограмма ожидала один параметр в виде названия файла. \nВы передали %s' % str(len(argv)-1))

if __name__ == '__main__':
    anagram()