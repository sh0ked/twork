# coding=utf-8
from sys import argv
import itertools

ALPHABET = 'abcdefghijklmnopqrstuvwxyz'


def get_phases(length, str_=None):
    if str_ is not None:
        alphabet = ALPHABET[ALPHABET.index(str_[0]):ALPHABET.index(str_[len(str_)-1])+1]
    else:
        alphabet = ALPHABET

    # print 'Alphabet:', alphabet
    return [''.join(i) for i in itertools.product(alphabet, repeat=length)]


def pairs():
    if len(argv) == 3:
        
        first_str = argv[1].lower()
        second_str = argv[2].lower()

        if first_str.isalpha() and second_str.isalpha():
            if len(first_str) == len(second_str):
                new_str = sorted(list(set(first_str + second_str)))
                print 'Phrases:', first_str, second_str

                lst = get_phases(len(first_str), new_str)

                print 'Results:'
                for l in lst:
                    if first_str <= l <= second_str:
                        print '-', l

            elif len(first_str) < len(second_str):
                print 'Phrases:', first_str, second_str

                lst_f = get_phases(len(first_str))
                lst_s = get_phases(len(second_str))

                print 'Results:'
                for l in lst_f:
                    if first_str <= l <= 'z'*len(first_str):
                        print '-', l
                for l in lst_s:
                    if 'a'*len(second_str) <= l <= second_str:
                        print '-', l

            else:
                print 'Phrases:', second_str, first_str

                lst_f = get_phases(len(second_str))
                lst_s = get_phases(len(first_str))

                print 'Results:'
                for l in lst_f:
                    if second_str <= l <= 'z'*len(second_str):
                        print '-', l
                for l in lst_s:
                    if 'a'*len(first_str) <= l <= first_str:
                        print '-', l
        else:
            raise AttributeError('\nАтрибуты имеют недопустимые символы т.е. не являются смволами латинского алфвита.\n'
                                 'Введите другие фразы в качестве параметров.')
    else:
        raise AttributeError('\nПрограмма ожидала два аргумента, т.е. две строки. \nВы передали %s' % str(len(argv)-1))

if __name__ == '__main__':
    pairs()